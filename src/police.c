/*
  iamhisoka
*/

#include "../include/shm.h"

int main(int ac, char *av[])
{
    char *tab = NULL;
    size_t a = 0;
    ssize_t s = 0;
    FILE *fd;

    if (ac > 1) {
  	    printf("usage: %s\n", av[0]);
	      exit (1);
    }
    fd = fopen(".annuaire", "r");
    for (; s != -1;) {
        tab = NULL;
        s = getline(&tab, &a, fd);
        (s > 0) ? tab[s - 1] = '\0' : tab[s - 1];
	      if (s > 0)
	          printf("%s\n", tab);
      	free(tab);
    }
    fclose(fd);
    return (0);
}
